question - Get a random question
word - Get a random word
words - Get n random words
kys - Kill yourself
coin - Tosses a coin
wiki - Search Wikipedia
arch_wiki - Search the Arch wiki
insult - As expected, insults
weebify - Weebifies the given text
absurdify - mAke tExT aBSUrd
is - Is <your question>
are - Are <your question>
can - Can <your question>
will - will <your question>
shall - shall <your question>
was - Was <your question>
do - Do <your question>
does - Does <your question>
did - Did <your question>
should - Should <your question>
help - Need help? Go here
feedback - Send feedback, suggestion for kys, insult text
rate - Rate me on TGDR
